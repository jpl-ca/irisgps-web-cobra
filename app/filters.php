<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	Log::info('===========================================');
	Log::info('esta llegando como request: '. implode(',', $request->all()));

	$_content_type = Request::header('Content-Type');
	$_device_type = Request::header('Device-Type');
	$_imei = Request::header('IMEI');
	$_gcm = Request::header('gcm');

	Log::info('REQUEST Content-Type: '.$_content_type);
	Log::info('REQUEST Device-Type: '.$_device_type);
	Log::info('REQUEST IMEI: '.$_imei);
	Log::info('REQUEST GCM: '.$_gcm);

	Log::info('===========================================');
	
	if(!is_null($request->header('Device-Type')))
	{
		$responsor = new Responsor;

		if($request->header('Device-Type') == 'ws')
		{
			if (!is_null(Request::header('IMEI')) && !is_null(Request::header('gcm')))
			{
				$responsor->isNoAuth();

				$device = Device::findByMobile(Request::header('IMEI'));

				if(is_null($device))
				{
					$responsor->message = 'el dispositivo no está registrado';
					return $responsor->response();
				}

				if($device->isActivated())
				{
					if(!$device->isAllowed())
					{
						$responsor->message = 'el dispositivo no está permitido';
						return $responsor->response();
					}

					Device::registerGCM(Request::header('IMEI'));

					//->ejecuta la acción correspondiente

				}else{
					$responsor->message = 'el dispositivo no está activado';
					return $responsor->response();
				}
			}
			else
			{
				$responsor->isInvalid();
				$responsor->message = 'el IMEI del teléfono o id gcm no han sido encontrado en la solicitud';
				return $responsor->response();
			}
		}
		//->ES MO
	}
	//->ES WEB
});


App::after(function($request, $response)
{	
	if($request->header('Device-Type') == 'ws')
	{
		$token = null;
		if(Auth::check())
		{
			if(Auth::user()->hasDevice())
			{
				if(!($response->getData()->empty_token)) {
					$token = Auth::user()->device->generateToken();
				}
			}
		}
		$response->headers->set('token', $token);
	}
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		return Redirect::route('getLogin');
	}
});

Route::filter('admin', function()
{
	if (Auth::user()->isUser())
	{
		return Redirect::route('getEditProfile');
	}
});

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::route('getHome');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

include 'filters/app_filters.php';