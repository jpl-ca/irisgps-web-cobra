<?php

class LocationHistoryService {

	public static function storeLocations($locations)
	{
		$responsor = new Responsor;
		if(is_null($locations))
		{
			$responsor->message =  "los datos no están completos";
			return $responsor->response();
		}

		$locations = json_decode($locations, true);
		$locationHistories = LocationHistory::toSaveModel('LocationHistory', $locations);

		$responsor->message = "las posiciones se han registrado con éxito";
		return $responsor->response();
	}

}