<?php

class UserService {

	public static function login($email, $password)
	{	
		if(Auth::guest())
		{
			$user = array(
	            'email' => $email,
	            'password' => $password
	        );

	        $code = 0;

	        if (Auth::validate($user)) {

	        	$isWS = false;
	        	$via_remember = false;

	        	if(Request::header('Device-Type') == 'ws')
				{
		        	$isWS = true;
		        	$via_remember = true;

					$user = array(
			            'email' => $email,
			            'password' => $password,
			            'user_type_id' => 3
			        );

		        }else{

					$user = array(
			            'email' => $email,
			            'password' => $password
			        );
		        }

	        	if (Auth::attempt($user, $via_remember))
				{
					if($isWS){ Auth::user()->pairDevice(); }
					return $code = 2;
				}
				
				return $code = 1;
	        }
	        else {
	        	//return 'datos erroneos';
				return $code;
	        }
    	}
    	return false;
	}

	public static function logout()
	{
		if(Auth::check()){
			Auth::user()->unpairDevice();
			Auth::logout();
			//return 'logout';//Redirect::action('SiteController@getLogin');
			return true;
		}
		
		return false;
	}


	public static function authCheck()
	{
		return (Auth::check() || Auth::viaRemember());
	}

	public static function usersLastLocations()
	{
		/*$data = Vehicle::with(['locations' => function ($q) {
		  $q->orderBy('location_histories.created_at', 'DESC')->first();
		}])->get();*/
		$data = User::all();
		foreach($data as $v)
		{
		    $v->load(array('locationHistories'=>function($q){
		  		$q->orderBy('location_histories.created_at', 'DESC')->first();
			}));
		}

		if(is_null($data)){
			return Response::invalid(null, true, ": no hay vehículos que buscar"); //retorna un response invalido pero no actualiza el Token
		}

		return Response::success($data, false, false, ': toda la información de ruta actual se ha recuperado con éxito'); //retorna un response correcto y genera un Token nuevo
	}


	public static function userLastTrackingRoute($user_id)
	{
		/*$data = Vehicle::with(['locations' => function ($q) {
		  $q->orderBy('location_histories.created_at', 'DESC')->first();
		}])->get();*/

		$data = User::with(['locationHistories' => function ($q) {
			$q->orderBy('location_histories.created_at', 'DESC')->first();
		},	'trackingRoutes' => function ($q) {
			$q->orderBy('tracking_routes.date', 'DESC')->first();
		},
			'trackingRoutes.tasks.state',
			'trackingRoutes.tasks.customer',
			'trackingRoutes.tasks.stateHistory.state',
			'trackingRoutes.comments.user',
			'trackingRoutes.locationHistories'
		])->find($user_id);

		if(is_null($data)){
			return Response::invalid(null, true, ": no hay vehículos que buscar"); //retorna un response invalido pero no actualiza el Token
		}

		return Response::success($data, false, false, ': toda la información de ruta actual se ha recuperado con éxito'); //retorna un response correcto y genera un Token nuevo
	}

	public static function allUsers()
	{
		return $data = User::all();
	}

	public static function createUser($input)
	{
		$validator = Validator::make(
		    $input,
		    User::$rules
		);
		if ($validator->fails())
		{
			$messages = $validator->errors()->toArray();
			return Response::invalid(false, false, ": los datos tienen errores", $messages);
		}

		$data =new User;
		$data->first_name = $input['first_name'];
		$data->last_name = $input['last_name'];
		$data->email = $input['email'];
		$data->user_type_id = $input['user_type_id'];
		$data->password = Hash::make($input['password']);

		if($data->save())
		{
			return true;
		}

		return false;
	}

}