@extends('layouts.dashboard-base')

@section('content')
	<div class="row">
		<div class="col-sm-12">

        	<div class="col-lg-4 col-md-6">
                <div class="panel panel-iris">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                                <i class="fa fa-map-marker fa-5x"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge">Geo</div>
                                <div>Localización</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('getVehiclesPositions') }}">
                        <div class="panel-footer">
                            <span class="pull-left">Localización de Usuarios</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
		
			<div class="col-lg-4 col-md-6">
                <div class="panel panel-iris">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                                <i class="fa fa-male fa-5x"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge">Clientes</div>
                                <div>Ubicaciones y Datos</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('getAllCustomers') }}">
                        <div class="panel-footer">
                            <span class="pull-left">Gestionar</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
		
			<div class="col-lg-4 col-md-6">
                <div class="panel panel-iris">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                                <i class="fa fa-mobile fa-5x"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge">Dispositivos</div>
                                <div>Activación y Permisos</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('getAllDevices') }}">
                        <div class="panel-footer">
                            <span class="pull-left">Ver Detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
		
			<div class="col-lg-4 col-md-6">
                <div class="panel panel-iris">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                                <i class="fa fa-users fa-5x"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge">Usuarios</div>
                                <div>Cuentas y Gestión</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('getAllUsers') }}">
                        <div class="panel-footer">
                            <span class="pull-left">Administrar</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        
            <div class="col-lg-4 col-md-6">
                <div class="panel panel-iris">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                                <i class="fa fa-road fa-5x"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge">Visitas</div>
                                <div>Clientes y Organización</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('getAllTrackingRoutes') }}">
                        <div class="panel-footer">
                            <span class="pull-left">Gestionar</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
		
			<div class="col-lg-4 col-md-6">
                <div class="panel panel-iris">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-2">
                                <i class="fa fa-bar-chart fa-5x"></i>
                            </div>
                            <div class="col-xs-10 text-right">
                                <div class="huge">Reportes</div>
                                <div>Visitas por Semana</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('getVisitsPerWeek') }}">
                        <div class="panel-footer">
                            <span class="pull-left">Ver</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

		</div>
	</div>
@stop