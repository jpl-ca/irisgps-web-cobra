@extends('layouts.dashboard-base')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="hidden" id="current_date" date="{{Carbon::now()}}"></div>	
			{{$templator->getForm()}}
		</div>
	</div>
	@if(!is_null($data))
		@include('site.tracking-routes.display-all')
	@endif
@stop		

@section('js')
	{{$templator->getScripts()}}	
    @include('javascript.datatable-init')
    <script>
    	$(function () {
    		var now = $('#current_date').attr('date');
    		
            $('#from').datetimepicker({
                locale: 'es',
                format: 'DD/MM/YYYY',
                /*minDate: "2015-06-12",*/
                sideBySide: true,
                defaultDate: now
            });

            $('#until').datetimepicker({
                locale: 'es',
                format: 'DD/MM/YYYY',
                /*minDate: "2015-06-12",*/
                sideBySide: true,
                defaultDate: now
            });
        });
    </script>
@stop