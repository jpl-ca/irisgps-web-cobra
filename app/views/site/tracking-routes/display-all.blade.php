<div class="row">
	<div class="col-lg-12">
	    <table class="table table-striped table-bordered table-hover" id="all-elements-datatable">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Fecha de Ejecución</th>
                    <th>Usuario</th>
                    <th>Clientes</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $tracking_route)
                <tr class="">
                    <td>{{$tracking_route->id}}</td>
                    <td>{{$tracking_route->getDate()}}</td>
                    <td>{{$tracking_route->user->getFullname()}}</td>
                    <td>{{count($tracking_route->tasks)}}</td>
                    <td class="center">
                        <a class="iris-link" href="{{ route('getTrackingRouteById', $tracking_route->id) }}" title="ver">
                            <i class="fa fa-eye fa-fw"></i>
                        </a>   
                        <a class="iris-link confirm-action" href="{{ route('delete', array(get_class($tracking_route),$tracking_route->id)) }}" title="eliminar">
                            <i class="fa fa-trash fa-fw"></i>
                        </a>                         
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
	</div>
</div>