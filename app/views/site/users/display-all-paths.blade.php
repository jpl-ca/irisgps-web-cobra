<div class="row">
	<div class="col-lg-12">
	    <table class="table table-striped table-bordered table-hover" id="all-elements-datatable">
            <thead>
                <tr>
                    <th>Fecha de Ejecución</th>
                    <th>Usuario</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $user_path)
                <tr class="">
                    <td>{{$user_path->getDate('created_at')}}</td>
                    <td>{{$user_path->user->getFullname()}}</td>
                    <td class="center">
                        <a class="iris-link" href="{{ route('getPathByUserIdAndDate', [$user_path->user_id, $user_path->getDate('created_at')]) }}" title="ver">
                            <i class="fa fa-eye fa-fw"></i>
                        </a>                       
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
	</div>
</div>