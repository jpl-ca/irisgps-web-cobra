@extends('layouts.base')

@section('header')
	<style type="text/css">

		.vertical-center {
			min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
			min-height: 100vh; /* These two lines are counted as one :-)       */

			display: flex;
			align-items: center;
		}

		.img-responsive {
			margin: 0 auto;
		}

		.error-code{
			font-size: 100px;
		}

		.iris-primary{
			color: #1565C0;
		}

		.btn-iris {
			color: #1565C0;
			border: 2px solid #1565C0;
		}

		.btn-iris:hover {			
			color: #fff;
			background-color: #1565C0;
		}
		
	</style>
@stop

@section('body')
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="vertical-center">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<p class="text-center error-code iris-primary">404!</p>
								<p class="text-center"><img class="img img-responsive" src="{{ asset('assets/images/inicio_indigo.png') }}" alt="IrisGPS"></p>
								<h3 class="text-center iris-primary">Lo sentimos la pagina que esta buscando no existe</h3>
								<p class="text-center"><a href="{{URL::previous()}}" class="btn btn-iris btn-lg"><i class="fa fa-home"></i> Volver</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop