@extends('layouts.base')

@section('body')

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-iris navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('getHome') }}"><img src="{{asset('assets/images/irisgps-logo-largo_inv.png')}}"  class="img text-center" alt="" style="height:40px"></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                {{--
                <li class="dropdown">
                    <a class="menu-link dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="menu-link dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 1</strong>
                                        <span class="pull-right text-muted">40% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 2</strong>
                                        <span class="pull-right text-muted">20% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 3</strong>
                                        <span class="pull-right text-muted">60% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 4</strong>
                                        <span class="pull-right text-muted">80% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="menu-link dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                --}}
                <li class="dropdown">
                    <a class="menu-link dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="javascript:void(0)">{{Auth::user()->getFullname()}}</a>
                        <li><a href="{{ route('getEditProfile') }}"><i class="fa fa-user fa-fw"></i> Perfil</a>
                        </li>
                        {{--
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Configuración</a>
                        </li>
                        --}}
                        <li class="divider"></li>
                        <li><a href="{{route('getLogout')}}"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesión</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        {{--
                        <li class="sidebar-search">
                            <div class="text-center">
                                <img src="{{ asset('assets/images/cobra_40.png') }}" alt="">
                            </div>
                        </li>
                        --}}
                        {{--
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        --}}
                        @if(Auth::user()->isUser())
                        <li {{ (Request::is('perfil')) ? 'class="active"' : ''}} >
                            <a href="{{route('getEditProfile')}}" class="{{ (Request::is('perfil')) ? 'active' : ''}}"><i class="fa fa-user fa-fw"></i> Perfil</a>
                        </li>
                        @else
                        <li {{ (Request::is('/')) ? 'class="active"' : ''}} >
                            <a href="{{route('getHome')}}" class="{{ (Request::is('/')) ? 'active' : ''}}"><i class="fa fa-home fa-fw"></i> Home</a>
                        </li>
                        <li {{ (Request::is('usuarios*')) ? 'class="active"' : ''}} >
                            <a href="#" class="{{ (Request::is('usuarios/*')) ? 'active' : ''}}"><i class="fa fa-users fa-fw"></i> Usuarios<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('getVehiclesPositions') }}" class="{{ (Request::is('usuarios/localizacion')) ? 'active' : ''}}">Ver Localización</a>
                                </li>
                                <li {{ (Request::is('usuarios/todo') || Request::is('usuarios/agregar')) ? 'class="active"' : ''}} >
                                    <a href="#" {{ (Request::is('usuarios/todo') || Request::is('usuarios/agregar')) ? 'class="active"' : ''}}>Gestionar Usuarios <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{{ route('getAllUsers') }}" {{ (Request::is('usuarios/todo')) ? 'class="active"' : ''}}>Ver Todos</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('getCreateUser') }}"{{ (Request::is('usuarios/agregar')) ? 'class="active"' : ''}}>Agregar Nuevo</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                                <li {{ (Request::is('usuarios/buscar-recorridos') || Request::is('usuarios/recorrido-de-usuario*')) ? 'class="active"' : ''}} >
                                    <a href="#" {{ (Request::is('usuarios/buscar-recorridos') || Request::is('usuarios/recorrido-de-usuario*')) ? 'class="active"' : ''}}>Gestionar Recorridos <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{{ route('getSearchUserPath') }}" {{ (Request::is('usuarios/buscar-recorridos')) ? 'class="active"' : ''}}>Buscar Recorridos</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('dispositivos*')) ? 'class="active"' : ''}} >
                            <a href="#" {{ (Request::is('dispositivos*')) ? 'class="active"' : ''}}><i class="fa fa-mobile fa-fw"></i> Gestionar Dispositivos<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('getAllDevices') }}" {{ (Request::is('dispositivos/todo')) ? 'class="active"' : ''}}>Ver Todos</a>
                                </li>
                                <li>
                                    <a href="{{ route('getCreateDevice') }}" {{ (Request::is('dispositivos/agregar')) ? 'class="active"' : ''}}>Agregar Nuevo</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('visitas*')) ? 'class="active"' : ''}} >
                            <a href="#" {{ (Request::is('visitas*')) ? 'class="active"' : ''}}><i class="fa fa-road fa-fw"></i> Gestionar Visitas<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('getSearchTrackingRoute') }}" {{ (Request::is('visitas/buscar')) ? 'class="active"' : ''}}>Buscar Visitas</a>
                                </li>
                                <li>
                                    <a href="{{ route('getAllTrackingRoutes') }}" {{ (Request::is('visitas/todo')) ? 'class="active"' : ''}}>Ver Todos</a>
                                </li>
                                <li>
                                    <a href="{{ route('getCreateTrackingRoute') }}" {{ (Request::is('visitas/agregar')) ? 'class="active"' : ''}}>Agregar Nuevo</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('clientes*')) ? 'class="active"' : ''}} >
                            <a href="#" {{ (Request::is('clientes*')) ? 'class="active"' : ''}}><i class="fa fa-male fa-fw"></i> Gestionar Clientes<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('getAllCustomers') }}" {{ (Request::is('clientes/todo')) ? 'class="active"' : ''}}>Ver Todos</a>
                                </li>
                                <li>
                                    <a href="{{ route('getCreateCustomer') }}" {{ (Request::is('clientes/agregar')) ? 'class="active"' : ''}}>Agregar Nuevo</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('reportes*')) ? 'class="active"' : ''}} >
                            <a href="#" {{ (Request::is('reportes*')) ? 'class="active"' : ''}}><i class="fa fa-bar-chart fa-fw"></i> Reportes<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('getVisitsPerWeek') }}" {{ (Request::is('reportes/visitas-por-semana')) ? 'class="active"' : ''}}>Visitas por Semana</a>
                                </li>
                                <li>
                                    <a href="{{ route('getIncidentsPerWeek') }}" {{ (Request::is('reportes/incidentes-por-semana')) ? 'class="active"' : ''}}>Incidentes por Semana</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        @endif
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <!--ALERT ZONE -->
                        @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{Session::get('success')}}
                        </div>
                        @endif
                        @if(Session::has('info'))
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{Session::get('info')}}
                        </div>
                        @endif
                        @if(Session::has('warning'))
                        <div class="alert alert-warning alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{Session::get('warning')}}
                        </div>
                        @endif
                        @if(Session::has('error'))
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{Session::get('error')}}
                        </div>
                        @endif
                        @if(Session::has('validation_errors'))
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Se han encontrado los siguientes errores:
                            <ul>
                                @foreach (Session::get('validation_errors') as $validation_error)
                                    @foreach ($validation_error as $error_message)
                                        <li>{{$error_message}}</li>
                                    @endforeach
                                @endforeach
                            </ul>
                        </div>
                        @endif                      
                        <!--END ALERT ZONE -->
                        <h1 class="page-header">{{ isset($pageTitle) ? $pageTitle : 'Título de la página' }}
                            @if(isset($actionButton))
                                <a href="{{ route($actionButton['route']) }}" class="btn btn-{{ $actionButton['class'] }} pull-right">{{ $actionButton['title'] }}</a>
                            @endif
                        </h1>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12">
                                    @yield('content')
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
@stop