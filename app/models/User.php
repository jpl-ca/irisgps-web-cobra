<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	// Add your validation rules here
	public static $rules = [
		'first_name' => 'required',
		'last_name' => 'required',
		'email' => 'required|email|unique:users,email',
		'user_type_id' => 'required|numeric',
		'password' => 'required',
	];

	public function getUpdateRules()
	{
		return 	$rules = [
			'first_name' => 'required',
			'last_name' => 'required',
			'email' => 'required|email|unique:users,email'.( is_null($this->id) ? '' : ','.$this->id ),
			'user_type_id' => 'required|numeric',
			'password' => ''
		];
	}

	public function isUser()
	{
		return ($this->user_type_id == 3);
	}

	public function getRememberToken()
    {   
    return $this->remember_token;
    }

    public function setRememberToken($value)
    {
    $this->remember_token = $value;
    }   

    public function getRememberTokenName()
    {
    return 'remember_token';
    }

	public static function getUserList()
	{
		return User::select(DB::raw('id, concat(first_name, " ", last_name) as full_name'))->lists('full_name', 'id');
	}

	public function hasDevice()
	{
		return (is_null($this->device_id)) ? false : true;
	}

	public function pairDevice()
	{
		$device = Device::findByMobile(Request::header('IMEI'));
		$this->device_id = $device->id;
		$this->save();
	}

	public function unpairDevice()
	{
		if($this->hasDevice())
		{
			$this->device->removeToken();
			$this->device_id = null;
			$this->save();
		}
	}

	public function getFullName()
	{
		return $this->first_name.' '.$this->last_name;
	}

    public function device()
    {
        return $this->belongsTo('Device','device_id','id');
    }

    public function type()
    {
        return $this->belongsTo('UserType','user_type_id','id');
    }    

    public function trackingRoutes()
    {
        return $this->hasMany('TrackingRoute','user_id','id');
    }

    public function locationHistories()
    {
        return $this->hasManyThrough('LocationHistory', 'TrackingRoute', 'user_id', 'tracking_route_id');
    }

    public function hasTrackingRoute($date = null)
    {
    	$date = is_null($date) ? Carbon::now() : Carbon::createFromFormat('d-m-Y', $date);
    	$tracking_route = TrackingRoute::where('date', '>=', $date->startOfDay()->toDateTimeString())
		    ->where('date', '<=', $date->endOfDay()->toDateTimeString())
		    ->where('user_id', $this->id)
		    ->first();

		return (!is_null($tracking_route)) ? $this->getTrackingRouteById($tracking_route->id) : null;
    }

    public function getTrackingRouteById($tracking_route_id)
    {
    	return TrackingRoute::with([
			'tasks.state',
			'tasks.customer',
			'tasks.stateHistory.state',
			'comments.user'
		])->find($tracking_route_id);
    }

    public function hasTrackingRouteLocationHistory($date = null)
    {
    	$date = is_null($date) ? Carbon::now() : Carbon::createFromFormat('d-m-Y', $date);
    	$tracking_route = TrackingRoute::where('date', '>=', $date->startOfDay()->toDateTimeString())
		    ->where('date', '<=', $date->endOfDay()->toDateTimeString())
		    ->where('user_id', $this->id)
		    ->first();

		return (!is_null($tracking_route)) ? $this->getLocationHistoryByTrackingRouteId($tracking_route->id) : null;
    }

    public function getLocationHistoryByTrackingRouteId($tracking_route_id)
    {
    	return LocationHistory::leftJoin('tracking_routes', 'tracking_routes.id', '=', 'location_histories.tracking_route_id')
    	->with([
			'incidentType'
		])
		->where('location_histories.tracking_route_id', $tracking_route_id)
		->where('tracking_routes.user_id', $this->id)
		->orderBy('location_histories.created_at', 'DESC')
		->select('location_histories.*')
		->get();
    }

    public function hasLocationHistory($date = null)
    {
    	$location_history = $this->getLocationHistoryByDate($date);

		return (!is_null($location_history->first())) ? $location_history : null;
    }

    public function getLocationHistoryByDate($date = null)
    {
    	$date = is_null($date) ? Carbon::now() : Carbon::createFromFormat('d-m-Y', $date);
    	return LocationHistory::leftJoin('tracking_routes', 'tracking_routes.id', '=', 'location_histories.tracking_route_id')
    	->with([
			'incidentType'
		])
		->where('tracking_routes.user_id', $this->id)
		->where('location_histories.created_at', '>=', $date->startOfDay()->toDateTimeString())
		->where('location_histories.created_at', '<=', $date->endOfDay()->toDateTimeString())
		->orderBy('location_histories.created_at', 'DESC')
		->select('location_histories.*')
		->get();
    }

    public function hasLastPosition()
    {
    	$last_position = LocationHistory::with([
			'incidentType'
		])
		->where('user_id', $this->id)
		->orderBy('created_at', 'DESC')
		->first();

		return (!is_null($last_position)) ? $last_position : null;
    }

}
