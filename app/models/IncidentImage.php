<?php

class IncidentImage extends IrisModel {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];


	public function locationHistory()
    {
        return $this->belongsTo('LocationHistory', 'location_history_id','id');
    }

}