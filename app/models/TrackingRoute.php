<?php

class TrackingRoute extends IrisModel {

	// Add your validation rules here
    public static $native_date = [
        'date' => 'required|date_format:d/m/Y'
    ];

    public static $rules = [
        'date' => 'required|date_format:Y-m-d H:i:s|unique_with:tracking_routes,user_id',
        'user_id' => 'required|numeric',
        'customers' => 'required|array|between:1,7'
    ];

	// Don't forget to fill this array
	protected $fillable = [];

    public function getUpdateRule(){
        return $rules = [
            'date' => 'required|date_format:d/m/Y|unique_with:tracking_routes,user_id'.( is_null($this->id) ? '' : ','.$this->id ),
            'user_id' => 'required|numeric',
            'customers' => 'required|array|between:1,7'
        ];
    }

    public function getDate(){
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->date)->format('d/m/Y H:i:s');
    }

    public function customers()
    {
        return $this->belongsToMany('Customer', 'route_tasks');
    }

	public function tasks()
    {
        return $this->hasMany('RouteTask','tracking_route_id','id');
    }

    public function comments()
    {
        return $this->hasMany('RouteComment','tracking_route_id','id');
    }

    public function locationHistories()
    {
        return $this->hasMany('LocationHistory','tracking_route_id','id');
    }

	public function lastLocation()
    {
        return $this->hasMany('LocationHistory')->orderBy('created_at', 'DESC')->take(1);
    }

    public function user()
    {
        return $this->belongsTo('User','user_id','id');
    }

    public function hasLastPosition()
    {
        $last_position = LocationHistory::leftJoin('tracking_routes', 'tracking_routes.id', '=', 'location_histories.tracking_route_id')
        ->with([
            'incidentType'
        ])
        ->where('tracking_routes.id', $this->id)
        ->orderBy('location_histories.created_at', 'DESC')
        ->select('location_histories.*')
        ->first();

        return (!is_null($last_position)) ? $last_position : null;
    }

}