<?php

class AuthController extends \BaseController {

	public function postLogin()
	{
		$email = Input::get('email');
		$password = Input::get('password');

		$result = UserService::login($email, $password);
		
		if(!$result)
		{
			if(JMUserAgent::isWebRequest()){
				return Redirect::back()->withError('Usuario o contraseña incorrectos')->withInput();
			}
			$responsor = new Responsor;
			$responsor->isInvalid();
			$responsor->message = "no se pudo procesar la solicitud";
			$responsor->data = "Usuario o contraseña incorrectos";
			return $responsor->response();
		}else{
			if(JMUserAgent::isWebRequest()){
				if($result == 2)
				{
					return Redirect::route('getHome')->withSuccess('Se ha iniciado sesión correctamente');
				}else{
					return Redirect::back()->withError('No tiene autorización')->withInput();
				}
			}
			$responsor = new Responsor;
			$responsor->isSuccess();
			$responsor->data = Auth::user()->toArray();
			$responsor->message = "se ha iniciado la sesión correctamente";
			return $responsor->response();
		}
	}

	public function getLogout()
	{
		$result = UserService::logout();

		if(!$result)
		{
			if(JMUserAgent::isWebRequest()){
				return Redirect::back()->withError('No existe una sesión que cerrar')->withInput();					
			}
			$responsor = new Responsor;
			$responsor->isInvalid();
			$responsor->message = "no existe una sesión que cerrar";
			$responsor->data = "No existe una sesión que cerrar";
			return $responsor->response();
		}else{
			if(JMUserAgent::isWebRequest()){
				return Redirect::action('SiteController@getLogin')->withSuccess('Se ha cerrado sesión correctamente');
			}
			$responsor = new Responsor;
			$responsor->isSuccess();
			$responsor->message = "se ha cerrado la sesión correctamente";
			return $responsor->response();
		}
	}

	public function getAuthCheck()
	{
		$responsor = new Responsor;
		$result = UserService::authCheck();

		if($result)
		{
			$message = 'si existe una sesión para el usuario.';
			$responsor->isSuccess();
		}else{
			$message = 'no existe una sesión para el usuario.';
			$responsor->isInvalid();
		}
		$responsor->message = $message;

		return $responsor->response();
	}

	/*
	| Web Service
	*/

	public function postPairDevice()
	{
		$mobile =  Request::header('IMEI');

		$plate = Input::get('plate');

		$dni = Input::get('dni');

		return DeviceService::pairDevice($mobile, $plate, $dni);
	}

	public function postUnpairDevice()
	{
		$mobile =  Request::header('IMEI');

		return DeviceService::unpairDevice($mobile);
	}

	public function delete($class, $id)
	{
		$object = $class::findOrFail($id);
		try {
			$object->delete();
			$done = true;
		} catch (Exception $e) {
			$done = false;
		}

		if($done)
		{
			return Redirect::back()->withSuccess('Se ha eliminado el registro correctamente.');
		}else{
			return Redirect::back()->withError('No se pudo eliminado el registro. Es posible que este siendo referenciado.');
		}
	}

}