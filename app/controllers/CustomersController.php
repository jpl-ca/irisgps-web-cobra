<?php

class CustomersController extends \BaseController {

	public function getAllCustomers()
	{
		$response = CustomerService::allCustomers();
		$data = $response['data'];

		$actionButton = array(
			'route'=>'getCreateCustomer',
			'title'=>'Agregar Nuevo',
			'class'=>'info'
		);

		return View::make('site.customers.all')->with(array('pageTitle'=>'Clientes', 'actionButton'=>$actionButton, 'data' => $data));
	}

	public function getCreateCustomer()
	{
		$actionButton = array(
			'route'=>'getAllCustomers',
			'title'=>'Regresar',
			'class'=>'default'
		);

		$templator = new Templator;

		$templator->setSize(7);
		$templator->createForm($actionRoute = 'postCreateCustomer', $routeParams = null, $method = 'POST', $legend = 'Registrar nuevo cliente', $submitName = 'Agregar', $resetName = null);
		$templator->addText($id = 'name', $label = 'Nombre*', $name = 'name', $placeholder = null, $required = true, $autocomplete=false, $helpblock = null);
		$templator->addText($id = 'customer_code', $label = 'Código de Cliente*', $name = 'customer_code', $placeholder = null, $required = true, $autocomplete=false, $helpblock = null);
		$templator->addText($id = 'address', $label = 'Dirección', $name = 'address', $placeholder = null, $required = false, $autocomplete=false, $helpblock = null);
		$templator->addText($id = 'phone', $label = 'Telf.*', $name = 'phone', $placeholder = null, $required = true, $autocomplete=false, $helpblock = null);
		$templator->addText($id = 'mobile', $label = 'Cel.', $name = 'mobile', $placeholder = null, $required = false, $autocomplete=false, $helpblock = null);
		$templator->addHidden($id = 'lat', $name = 'lat', $value = null);
		$templator->addHidden($id = 'lng', $name = 'lng', $value = null);

		return View::make('site.customers.create')->with(array('pageTitle'=>'Clientes', 'actionButton'=>$actionButton, 'templator' => $templator));
	}

	public function postCreateCustomer()
	{
		$input = Input::only('name', 'customer_code', 'address', 'phone', 'mobile', 'lat', 'lng');

		if(empty(trim($input['lat'])) || empty(trim($input['lng'])))
		{
			return Redirect::back()->withError('Debe de seleccionar una ubicación.')->withInput();				
		}

		$validator = Validator::make(
		    $input,
		    Customer::$rules
		);

		if ($validator->fails())
		{
			$messages = $validator->errors()->toArray();
			return Redirect::back()->withValidationErrors($messages)->withInput();
		}

		$data =new Customer;
		$data->customer_code = $input['customer_code'];
		$data->name = $input['name'];
		$data->phone = $input['phone'];
		$data->mobile = (empty(trim($input['mobile']))) ? null : $input['mobile'];
		$data->lat = $input['lat'];
		$data->lng = $input['lng'];

		$addressData = GeoPos::getAddressData($data->lat, $data->lng);

		$data->address = (empty(trim($input['address']))) ? $addressData['address'] : $input['address'];		
		$data->district = $addressData['district'];
		$data->province = $addressData['province'];
		$data->region = $addressData['region'];

		$data->save();

		//return Redirect::back()->withValidationError('Hubo un error al procesar la acción.')->withInput();					

		return Redirect::route('getAllCustomers')->withSuccess('Cliente registrado satisfactoriamente');

		return $response;
	}

	public function getEditCustomer($id)
	{
		$customer = Customer::findOrFail($id);

		$actionButton = array(
			'route'=>'getAllCustomers',
			'title'=>'Regresar',
			'class'=>'default'
		);

		$templator = new Templator;

		$templator->setSize(7);
		$templator->createForm($actionRoute = 'postEditCustomer', $routeParams = array($customer->id), $method = 'POST', $legend = 'Editar cliente', $submitName = 'Guardar', $resetName = null);
		$templator->addText($id = 'name', $label = 'Nombre*', $name = 'name', $placeholder = null, $required = true, $autocomplete=false, $helpblock = null, $value = $customer->name);
		$templator->addText($id = 'customer_code', $label = 'Código de Cliente*', $name = 'customer_code', $placeholder = null, $required = true, $autocomplete=false, $helpblock = null, $value = $customer->customer_code);
		$templator->addText($id = 'address', $label = 'Dirección', $name = 'address', $placeholder = null, $required = false, $autocomplete=false, $helpblock = null, $value = $customer->address);
		$templator->addText($id = 'phone', $label = 'Telf.*', $name = 'phone', $placeholder = null, $required = true, $autocomplete=false, $helpblock = null, $value = $customer->phone);
		$templator->addText($id = 'mobile', $label = 'Cel.', $name = 'mobile', $placeholder = null, $required = false, $autocomplete=false, $helpblock = null, $value = $customer->mobile);
		$templator->addHidden($id = 'lat', $name = 'lat', $value = $customer->lat);
		$templator->addHidden($id = 'lng', $name = 'lng', $value = $customer->lng);

		return View::make('site.customers.create')->with(array('pageTitle'=>'Clientes', 'actionButton'=>$actionButton, 'templator' => $templator));
	}

	public function postEditCustomer($id)
	{
		$customer = Customer::findOrFail($id);

		$input = Input::only('name', 'customer_code', 'address', 'phone', 'mobile', 'lat', 'lng');

		if(empty(trim($input['lat'])) || empty(trim($input['lng'])))
		{
			return Redirect::back()->withError('Debe de seleccionar una ubicación.')->withInput();				
		}

		$validator = Validator::make(
		    $input,
		    $customer->getUpdateRules()
		);

		if ($validator->fails())
		{
			$messages = $validator->errors()->toArray();
			return Redirect::back()->withValidationErrors($messages)->withInput();
		}

		$customer->customer_code = $input['customer_code'];
		$customer->name = $input['name'];
		$customer->phone = $input['phone'];
		$customer->mobile = (empty(trim($input['mobile']))) ? null : $input['mobile'];
		if(($customer->lat != $input['lat']) && ($customer->lng = $input['lng']))
		{
			$customer->lat = $input['lat'];
			$customer->lng = $input['lng'];

			$addressData = GeoPos::getAddressData($customer->lat, $customer->lng);

			$customer->address = $addressData['address'];		
			$customer->district = $addressData['district'];
			$customer->province = $addressData['province'];
			$customer->region = $addressData['region'];
		}		

		$customer->save();

		//return Redirect::back()->withValidationError('Hubo un error al procesar la acción.')->withInput();					

		return Redirect::route('getAllCustomers')->withSuccess('Cliente registrado satisfactoriamente');

		return $response;
	}
}