<?php

class TrackingRoutesController extends \BaseController {

	public function getAllTrackingRoutes()
	{
		$data = TrackingRouteService::allTrackingRoutes();

		$actionButton = array(
			'route'=>'getCreateTrackingRoute',
			'title'=>'Agregar Nuevo',
			'class'=>'info'
		);

		return View::make('site.tracking-routes.all')->with(array('pageTitle'=>'Visitas', 'actionButton'=>$actionButton, 'data' => $data));
	}

	public function getCreateTrackingRoute()
	{

		$actionButton = array(
			'route'=>'getAllTrackingRoutes',
			'title'=>'Regresar',
			'class'=>'default'
		);

		$templator = new Templator;

		$templator->createForm($actionRoute = 'postCreateTrackingRoute', $routeParams = null, $method = 'POST', $legend = 'Crear nueva ruta', $submitName = 'Agregar', $resetName = 'Reset');
		$templator->addText($id = 'date', $label = 'Fecha', $name = 'date', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
		$templator->addSelectBasic($id = 'user_id', $label = 'Usuario', $name = 'user_id', $elements = User::getUserList(), $haveEmptyOption = false);
		$templator->addDualListBox($id = 'customers', $label = 'Clientes', $name = 'customers', $elements = Customer::all()->lists('name', 'id'), $rows = 10, $fromName = 'Todos los clientes', $toName='Elegidos para visitar', $size = 7);
		//$templator->addLinkButton('postCreateVehicle', $params = null, $class = 'info', $name = 'Agregar Cliente <i class="fa fa-plus"></i>');

		return View::make('site.CRUD.basic-form')->with(array('pageTitle'=>'Visitas', 'actionButton'=>$actionButton, 'templator' => $templator));
	}

	public function getSearchTrackingRoute()
	{
		$data = null;

		if(Input::has('user_id'))
		{
			$user_id = Input::get('user_id');
			$from = Carbon::createFromFormat('d/m/Y', Input::get('from'))->startOfDay();
			$until = Carbon::createFromFormat('d/m/Y', Input::get('until'))->endOfDay();

			$data = TrackingRoute::where('date', '>=', $from)
				->where('date', '<=', $until);

			if($user_id != 0)
			{
				$data = $data->where('user_id', $user_id);
			}
			$data = $data->orderBy('date','DESC')->get();
		}

		$actionButton = array(
			'route'=>'getAllTrackingRoutes',
			'title'=>'Regresar',
			'class'=>'default'
		);

		$templator = new Templator;

		$templator->createForm($actionRoute = 'getSearchTrackingRoute', $routeParams = null, $method = 'GET', $legend = 'Buscar', $submitName = 'Buscar', $resetName = 'Reset');
		$templator->addText($id = 'from', $label = 'Desde', $name = 'from', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
		$templator->addText($id = 'until', $label = 'Hasta', $name = 'until', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
		$templator->addSelectBasic($id = 'user_id', $label = 'Usuario', $name = 'user_id', $elements = array('0'=>'Cualquiera')+User::getUserList(), $haveEmptyOption = false);
		//$templator->addLinkButton('postCreateVehicle', $params = null, $class = 'info', $name = 'Agregar Cliente <i class="fa fa-plus"></i>');

		return View::make('site.tracking-routes.search')->with(array('pageTitle'=>'Visitas', 'actionButton'=> null /*$actionButton*/, 'templator' => $templator, 'data' => $data));
	}	

	public function getSearchUserPath()
	{
		$data = null;

		if(Input::has('user_id'))
		{
			$user_id = Input::get('user_id');
			$from = Carbon::createFromFormat('d/m/Y', Input::get('from'))->startOfDay();
			$until = Carbon::createFromFormat('d/m/Y', Input::get('until'))->endOfDay();

			$data = LocationHistory::where('created_at', '>=', $from)
				->where('created_at', '<=', $until)
				->groupBy('user_id')
				->select( DB::raw( "DATE_FORMAT(created_at,'%Y-%m-%d') as created_at, user_id" ) );

			if($user_id != 0)
			{
				$data = $data->where('user_id', $user_id);
			}
			$data = $data->orderBy('created_at','DESC')->get();
		}

		//return dd($data->toArray());

		$actionButton = array(
			'route'=>'getAllUsers',
			'title'=>'Ver Usuarios',
			'class'=>'default'
		);

		$templator = new Templator;

		$templator->createForm($actionRoute = 'getSearchUserPath', $routeParams = null, $method = 'GET', $legend = 'Buscar', $submitName = 'Buscar', $resetName = 'Reset');
		$templator->addText($id = 'from', $label = 'Desde', $name = 'from', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
		$templator->addText($id = 'until', $label = 'Hasta', $name = 'until', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
		$templator->addSelectBasic($id = 'user_id', $label = 'Usuario', $name = 'user_id', $elements = array('0'=>'Cualquiera')+User::getUserList(), $haveEmptyOption = false);
		//$templator->addLinkButton('postCreateVehicle', $params = null, $class = 'info', $name = 'Agregar Cliente <i class="fa fa-plus"></i>');

		return View::make('site.users.path-search')->with(array('pageTitle'=>'Recorridos', 'actionButton'=> null /*$actionButton*/, 'templator' => $templator, 'data' => $data));
	}	

	public function postCreateTrackingRoute()
	{
		$date = Input::get('date');
		$user_id = Input::get('user_id');
		$customers = Input::get('customers');

		$customers = array_unique(is_null($customers) ? array() : $customers);

		$input = array(
			'date' => $date
		);

		$validator = Validator::make(
		    $input,
		    TrackingRoute::$native_date
		);
		
		if ($validator->fails())
		{
			$messages = $validator->errors()->toArray();
			//return Response::invalid(false, false, ": los datos tienen errores", $messages);
			return Redirect::back()->withValidationErrors($messages)->withInput();
		}
		
		$date = Carbon::createFromFormat('d/m/Y', $date)->startOfDay()->format('Y-m-d H:i:s');

		$input = array(
			'date' => $date,
			'user_id' => $user_id,
			'customers' => $customers
		);

		$validator = Validator::make(
		    $input,
		    TrackingRoute::$rules
		);

		if ($validator->fails())
		{
			$messages = $validator->errors()->toArray();
			//return Response::invalid(false, false, ": los datos tienen errores", $messages);
			return Redirect::back()->withValidationErrors($messages)->withInput();
		}

		$tracking_route = new TrackingRoute;
		$tracking_route->date = $date;
		$tracking_route->user_id = $user_id;
		$tracking_route->save();

		foreach ($customers as $customer_id) {
			$route_task = new RouteTask;
			$route_task->customer_id = $customer_id;
			$route_task->task_state_id	= 1;
			$route_task->tracking_route_id = $tracking_route->id;
			$route_task->description = 'Programado por el Sistema';
			$route_task->save();

			$task_state_history = new TaskStateHistory;
			$task_state_history->route_task_id = $route_task->id;
			$task_state_history->task_state_id = $route_task->task_state_id;
			$task_state_history->description = 'Programado por el Sistema';
			$task_state_history->save();
		}

		return Redirect::route('getCreateTrackingRoute')->withSuccess('Ruta creada correctamente.');
	}

	public static function getConfirmCreation($tracking_route_id)
	{
		$tracking_route = TrackingRoute::find($tracking_route_id);

		$elements = $tracking_route->employees()
				->select(DB::raw('concat(first_name, " ", last_name) as fullname, passengers.id'))
				->lists('fullname', 'id');

		$driver_id = $tracking_route->getDriverId();

		$templator = new Templator;

		$templator->createForm($actionRoute = 'postCreateTrackingRoute', $routeParams = null, $method = 'POST', $legend = 'Confirmar información de ruta', $submitName = 'Agregar', $resetName = 'Reset');
		$templator->addText($id = 'date', $label = 'Fecha', $name = 'date', $placeholder = 'dd/mm/YYYY', $required = true, $autocomplete=false, $helpblock = null);
		$templator->addRadioMultiple($id = 'casas', $name = 'casa', $label = 'Conductor', $elements, $checked = $driver_id);

		return View::make('site.tracking-routes.confirm-creation')->with(array('pageTitle'=>'Rutas', 'actionButton'=>null, 'templator' => $templator));
	}

	public static function getCurrentRouteInfo()
	{
		return TrackingRouteService::currentRouteInfo();
	}

	public static function getTrackingRouteById($tracking_route_id)
	{
		$response = TrackingRouteService::getTrackingRouteById($tracking_route_id);
		$data = $response['data'];
		return View::make('site.users.route')->with(array('pageTitle'=>'Visitas del Usuario', 'data' => $data));
	}

	public static function getPathByUserIdAndDate($user_id, $date)
	{
		$user = User::findOrFail($user_id);

		$data['user'] = $user;

		$response = TrackingRouteService::getPathByUserIdAndDate($user_id, $date);
		$data['path'] = json_encode( $response['data'] );
		$data['date'] =  $date;

		return View::make('site.users.path')->with(array('pageTitle'=>'Recorrido del Usuario', 'data' => $data));
	}

	public static function getRouteInfoByPlateAndDate($plate, $date = null)
	{
		if(is_null($date))
		{
			$response = TrackingRouteService::currentRouteInfo($plate);
		}else
		{
			$response = TrackingRouteService::getRouteInfoByPlateAndDate($plate, $date);
		}

		return $response;
	}

	public static function postCreateRouteComment()
	{
		$tracking_route_id = Input::get('tracking_route_id');
		$comment = Input::get('comment');

		$result = TrackingRouteService::registerComment($tracking_route_id, $comment);
		

		if(!$result)
		{
			if(JMUserAgent::isWebRequest()){
				return Redirect::back()->withError('Hubo un error al procesar la acción')->withInput();
			}
			$responsor = new Responsor;
			$responsor->isInvalid();
			$responsor->message = "no se pudo procesar la solicitud";
			return $responsor->response();
		}else{
			if(JMUserAgent::isWebRequest()){
				return Redirect::back()->withSuccess('Comentario creado satisfactoriamente');
			}
			$responsor = new Responsor;
			$responsor->isSuccess();
			$responsor->message = "se ha registrado el comentario con éxito";
			return $responsor->response();
		}
	}	

	public static function postChangeRouteTaskState()
	{
		$route_task_id = Input::get('route_task_id');
		$description = Input::get('description');
		$task_state_id = Input::get('task_state_id');

		$result = TrackingRouteService::changeRouteTaskState($route_task_id, $task_state_id, $description);

		if(!$result)
		{
			$responsor = new Responsor;
			$responsor->isInvalid();
			$responsor->message = "no se pudo procesar la solicitud";
			$responsor->data = "Datos inválidos";
			return $responsor->response();
		}else{
			$responsor = new Responsor;
			$responsor->isSuccess();
			$responsor->message = "se ha registrado el cambio de estado con éxito";
			return $responsor->response();
		}
	}	

	/*
	public function index()
	{
		$trackingroutes = Trackingroute::all();

		return View::make('trackingroutes.index', compact('trackingroutes'));
	}

	public function create()
	{
		return View::make('trackingroutes.create');
	}

	public function store()
	{
		$validator = Validator::make($data = Input::all(), Trackingroute::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Trackingroute::create($data);

		return Redirect::route('trackingroutes.index');
	}

	public function show($id)
	{
		$trackingroute = Trackingroute::findOrFail($id);

		return View::make('trackingroutes.show', compact('trackingroute'));
	}

	public function edit($id)
	{
		$trackingroute = Trackingroute::find($id);

		return View::make('trackingroutes.edit', compact('trackingroute'));
	}

	public function update($id)
	{
		$trackingroute = Trackingroute::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Trackingroute::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$trackingroute->update($data);

		return Redirect::route('trackingroutes.index');
	}

	public function destroy($id)
	{
		Trackingroute::destroy($id);

		return Redirect::route('trackingroutes.index');
	}
	*/

}
