<?php

class LocationHistoriesController extends \BaseController {

	public function postStoreLocations()
	{
		$locations = Input::get('locations');

		return LocationHistoryService::storeLocations($locations);		
	}

}
