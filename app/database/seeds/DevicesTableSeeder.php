<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class DevicesTableSeeder extends Seeder {

	public function run()
	{
		Device::create([
				'mobile' => '370997871904651',
				'allowed' => 1,
				'activated' => 1
			]);

		Device::create([
				'mobile' => '356521048573013',
				'allowed' => 1,
				'activated' => 1
			]);

		Device::create([
				'mobile' => '353111065354639',
				'allowed' => 1,
				'activated' => 1
			]);

		Device::create([
				'mobile' => '357957055448542',
				'allowed' => 1,
				'activated' => 1
			]);

		Device::create([
				'mobile' => '101010101010101010101',
				'allowed' => 1,
				'activated' => 1
			]);

		Device::create([
				'mobile' => '359288051429064',
				'allowed' => 1,
				'activated' => 1
			]);
		/*
		$faker => Faker::create();

		foreach(range(1, 10) as $index)
		{

		}
		*/
	}

}