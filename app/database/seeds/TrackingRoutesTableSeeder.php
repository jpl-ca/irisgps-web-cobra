<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class TrackingRoutesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(3, 12) as $user_id)
		{
			foreach(range(1, 150) as $routeId)
			{
				TrackingRoute::create([
					'date' => Carbon::now()->startOfDay()->addDays($routeId-1),
					'user_id' => $user_id
				]);
			}
		}
	}

}