<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrackingRoutesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tracking_routes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->dateTime('date');
			$table->integer('user_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();

			$table->unique(array('date','user_id'));
			$table->foreign('user_id')->references('id')->on('users');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tracking_routes');
	}

}
