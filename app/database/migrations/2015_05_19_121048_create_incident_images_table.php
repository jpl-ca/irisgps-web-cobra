<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIncidentImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('incident_images', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('url', 1000);
			$table->string('description', 2000);
			$table->integer('location_history_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('location_history_id')->references('id')->on('location_histories');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('incident_images');
	}

}
