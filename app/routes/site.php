<?php

/*
|--------------------------------------------------------------------------
| Web app Routes
|--------------------------------------------------------------------------
*/

//Route::get('/path', array('as'=>'', 'uses'=>''));

//Route::get('/', array('as'=>'getHome', 'uses'=>'SiteController@getHome'));

Route::group(array('before' => 'guest'), function()
{	
	Route::get('login', array('as'=>'getLogin', 'uses'=>'SiteController@getLogin'));

	Route::post('login', array('as'=>'postLogin', 'uses'=>'AuthController@postLogin'));

	Route::post('recupear-password', array('as'=>'postRemind', 'uses'=>'RemindersController@postRemind'));

	Route::get('cambiar-password/{token?}', array('as'=>'getReset', 'uses'=>'RemindersController@getReset'));

	Route::post('cambiar-password', array('as'=>'postReset', 'uses'=>'RemindersController@postReset'));
});


Route::group(array('before' => 'auth'), function()
{
	Route::get('logout', array('as'=>'getLogout', 'uses'=>'AuthController@getLogout'));

	Route::get('perfil', array('as'=>'getEditProfile', 'uses'=>'UsersController@getEditUser'));

	Route::post('perfil', array('as'=>'postEditProfile', 'uses'=>'UsersController@postEditUser'));

	Route::group(array('before' => 'admin'), function()
	{
		Route::get('/', array('as'=>'getHome', 'uses'=>'SiteController@getHome'));

		Route::group(array('prefix' => 'registro', 'before' => ''), function()
		{
			Route::get('eliminar/{class}/{id}', array('as'=>'delete', 'uses'=>'AuthController@delete'));

		});

		Route::group(array('prefix' => 'usuarios', 'before' => ''), function()
		{
			Route::get('localizacion', array('as'=>'getVehiclesPositions', 'uses'=>'UsersController@getUsersLastLocations'));

			Route::get('visitas-de-usuario/{tracking_route_id}', array('as'=>'getTrackingRouteById', 'uses'=>'TrackingRoutesController@getTrackingRouteById'));

			Route::get('recorrido-de-usuario/{user_id}/{date}', array('as'=>'getPathByUserIdAndDate', 'uses'=>'TrackingRoutesController@getPathByUserIdAndDate'));

			Route::get('buscar-recorridos', array('as'=>'getSearchUserPath', 'uses'=>'TrackingRoutesController@getSearchUserPath'));

			Route::get('/todo', array('as'=>'getAllUsers', 'uses'=>'UsersController@getAllUsers'));

			Route::get('agregar', array('as'=>'getCreateUser', 'uses'=>'UsersController@getCreateUser'));
			
			Route::post('agregar', array('as'=>'postCreateUser', 'uses'=>'UsersController@postCreateUser'));

			Route::get('editar/{id?}', array('as'=>'getEditUser', 'uses'=>'UsersController@getEditUser'));

			Route::post('editar/{id?}', array('as'=>'postEditUser', 'uses'=>'UsersController@postEditUser'));

		});

		Route::group(array('prefix' => 'reportes', 'before' => ''), function()
		{
			Route::get('visitas-por-semana', array('as'=>'getVisitsPerWeek', 'uses'=>'ReportsController@getVisitsPerWeek'));
			Route::get('incidentes-por-semana', array('as'=>'getIncidentsPerWeek', 'uses'=>'ReportsController@getIncidentsPerWeek'));

		});

		Route::group(array('prefix' => 'clientes', 'before' => ''), function()
		{
			Route::get('/todo', array('as'=>'getAllCustomers', 'uses'=>'CustomersController@getAllCustomers'));

			Route::get('agregar', array('as'=>'getCreateCustomer', 'uses'=>'CustomersController@getCreateCustomer'));
			
			Route::post('agregar', array('as'=>'postCreateCustomer', 'uses'=>'CustomersController@postCreateCustomer'));

			Route::get('editar/{id}', array('as'=>'getEditCustomer', 'uses'=>'CustomersController@getEditCustomer'));
			
			Route::post('editar/{id}', array('as'=>'postEditCustomer', 'uses'=>'CustomersController@postEditCustomer'));

		});

		Route::group(array('prefix' => 'dispositivos', 'before' => ''), function()
		{
			Route::get('/todo', array('as'=>'getAllDevices', 'uses'=>'DevicesController@getAllDevices'));

			Route::get('agregar', array('as'=>'getCreateDevice', 'uses'=>'DevicesController@getCreateDevice'));

			Route::post('agregar', array('as'=>'postCreateDevice', 'uses'=>'DevicesController@postCreateDevice'));

			Route::get('editar/{id}', array('as'=>'getEditDevice', 'uses'=>'DevicesController@getEditDevice'));

			Route::post('editar/{id}', array('as'=>'postEditDevice', 'uses'=>'DevicesController@postEditDevice'));
		});

		Route::group(array('prefix' => 'visitas', 'before' => ''), function()
		{
			Route::get('/todo', array('as'=>'getAllTrackingRoutes', 'uses'=>'TrackingRoutesController@getAllTrackingRoutes'));

			Route::get('agregar', array('as'=>'getCreateTrackingRoute', 'uses'=>'TrackingRoutesController@getCreateTrackingRoute'));

			Route::post('agregar', array('as'=>'postCreateTrackingRoute', 'uses'=>'TrackingRoutesController@postCreateTrackingRoute'));
			
			Route::get('confirmar/{tracking_route_id}', array('as'=>'getConfirmCreation', 'uses'=>'TrackingRoutesController@getConfirmCreation'));
			
			Route::post('agregar-comentario', array('as'=>'postCreateRouteComment', 'uses'=>'TrackingRoutesController@postCreateRouteComment'));

			Route::get('buscar', array('as'=>'getSearchTrackingRoute', 'uses'=>'TrackingRoutesController@getSearchTrackingRoute'));

		});
	});

});